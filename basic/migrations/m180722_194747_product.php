<?php

use yii\db\Migration;

/**
 * Class m180722_194747_product
 */
class m180722_194747_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
  
        $this->createTable('product', [
         
            'id' => $this->primaryKey(),
            'name' => $this->string().' NOT NULL',
            'cost' => $this->integer().' NOT NULL',
            'price' =>  $this->integer(). ' NOT NULL',
           
        ]
        );
  
  
  
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180722_194747_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180722_194747_product cannot be reverted.\n";

        return false;
    }
    */
}
