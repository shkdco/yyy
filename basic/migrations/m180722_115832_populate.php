<?php

use yii\db\Migration;

/**
 * Class m180722_115832_populate
 */
class m180722_115832_populate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('urgency',array(
            'id' =>'1',
            'urgency'=>'High',
        ));
        $this->insert('urgency',array(
            'id' =>'2',
            'urgency'=>'Medium',
        ));       
        $this->insert('urgency',array(
            'id' =>'3',
            'urgency'=>'Low',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180722_115832_populate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180722_115832_populate cannot be reverted.\n";

        return false;
    }
    */
}
