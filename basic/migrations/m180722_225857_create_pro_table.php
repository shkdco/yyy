<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pro`.
 */
class m180722_225857_create_pro_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pro', [
            'id' => $this->primarykey(),
            'name' => $this->string()->notNull(),
            'cost' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pro');
    }
}
