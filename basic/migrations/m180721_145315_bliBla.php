<?php

use yii\db\Migration;

/**
 * Class m180721_145315_bliBla
 */
class m180721_145315_bliBla extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            // 'id' => Schema::TYPE_PK,
            // 'name' => Schema::TYPE_STRING . ' NOT NULL',
            // 'userName' => Schema::TYPE_STRING . ' NOT NULL',
            // 'password' => Schema::TYPE_STRING . ' NOT NULL',
            // 'auth_key' => Schema::TYPE_STRING . ' NOT NULL',
        
            'id' => $this->primaryKey(),
            'name' => $this->string().' NOT NULL',
            'username' => $this->string().' NOT NULL',
            'password' =>  $this->string(). ' NOT NULL',
            'auth_key' => $this->string(). ' NOT NULL',
        ]);
        
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'name' => $this->string().' NOT NULL',
            'urgency' => $this->string().' NOT NULL',
        ]);

        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
            'urgency' => $this->string().' NOT NULL',
        ]);
 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180721_145315_bliBla cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180721_145315_bliBla cannot be reverted.\n";

        return false;
    }
    */
}
