<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use app\models\Product;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\db\ActiveRecord;
use yii\db\Query;

$this->title = 'Product';
$this->params['breadcrumbs'][] = $this->title;


$query = Product::find();



$provider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 3,
    ],
]);



// $provider = new ArrayDataProvider([
//    'allModels' => $model->getAll()
// ]);


?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    
    GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            'id',
            'price'
        ]
    ])
    
    
    ?> 


    <code><?= __FILE__ ?></code>
</div>