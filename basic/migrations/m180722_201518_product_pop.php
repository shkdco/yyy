<?php

use yii\db\Migration;

/**
 * Class m180722_201518_product_pop
 */
class m180722_201518_product_pop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//     ['id' =>1,'name'=>'Girafe','cost'=>10,'price'=>25 ],
//     ['id' =>2,'name'=>'Sandal','cost'=>5,'price'=>15 ],
//     ['id' =>3,'name'=>'Garlic','cost'=>0.1,'price'=>1 ],
//     ['id' =>7,'name'=>'Turbo','cost'=>3,'price'=>9 ],
        $this->insert('product',array(
            'id' =>'1',
            'name'=>'Girafe',
            'cost' =>'10',
            'price'=>'25',
        ));
        $this->insert('product',array(
            'id' =>'2',
            'name'=>'Sandal',
            'cost' =>'5',
            'price'=>'15',
        ));       
        $this->insert('product',array(
            'id' =>'3',
            'name'=>'Garlic',
            'cost' =>'0.1',
            'price'=>'1',
        ));
        $this->insert('product',array(
            'id' =>'5',
            'name'=>'Turbo',
            'cost' =>'3',
            'price'=>'9',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180722_201518_product_pop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180722_201518_product_pop cannot be reverted.\n";

        return false;
    }
    */
}
